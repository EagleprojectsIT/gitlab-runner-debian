FROM debian:stable-slim

RUN apt-get update && \
    apt-get install -y curl

RUN curl -fsSL get.docker.com | sh

RUN curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"

RUN apt install -fy ./gitlab-runner_amd64.deb

RUN apt-get clean

CMD ["gitlab-runner", "run"]
